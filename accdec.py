def linear(speed, acc_time, dec_time, total_time, freq):
  """ Returns a sequence of (duration, speed) that achieves the requested
      acceleration/deceleration profile.
  """
  flat_time = total_time - acc_time - dec_time
  if flat_time < 0:
    raise RuntimeError("Total time too small")

  acc_steps = round(acc_time * freq)
  flat_steps = round(flat_time * freq)
  dec_steps = round(dec_time * freq)

  for step in range(0, acc_steps):
    yield (1.0 / freq, speed / acc_steps * step)

  yield (flat_time, speed)

  for step in range(dec_steps - 1, -1, -1):
    yield (1.0 / freq, speed / dec_steps * step)
