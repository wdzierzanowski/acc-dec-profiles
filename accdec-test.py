#!/usr/bin/env python

import accdec
import itertools
import matplotlib.pyplot as plt

def main():
  f = 60.0
  t = 1.0
  durations, speeds = zip(*accdec.linear(
      speed=1.0, acc_time=.2, dec_time=.2, total_time=t, freq=f))
  times = list(itertools.accumulate(durations))
  times.insert(0, 0)
  plotdata = []
  for i in range(0, len(times) - 1):
    plotdata.append((times[i], speeds[i]))
    plotdata.append((times[i + 1], speeds[i]))
  x, y = zip(*plotdata)
  plt.plot(list(x), list(y))
  plt.show()

if __name__ == "__main__":
  main()
